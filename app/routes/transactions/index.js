import Ember from 'ember';

const {
  Route,
  get
} = Ember;

export default Route.extend({
    model() {
      // On initial load, fetch transactions from the past 30 days.
      const format = 'MM-DD-YYYY';
      const startDate = moment().add(-30, 'days').format(format);
      const endDate = moment().format(format);

      // store.query method is overriden to send a GET request at a custom URL.
      return get(this, 'store').query('transaction', { filterType: 'dateRange', startDate, endDate });
    },

    actions: {
      filter(keyword) {
        if (keyword !== '') {
          return get(this, 'store').filter('transaction', transaction => {
            const regex = new RegExp(keyword, 'i');

            return regex.test(transaction.get('account')) ||
              regex.test(transaction.get('description')) ||
              regex.test(transaction.get('category_name'));
          });
        } else {
          return get(this, 'store').filter('transaction', transaction => transaction);
        }
      },

      loadAccounts() {
        return get(this, 'store').findAll('account');
      },

      loadCategories() {
        return get(this, 'store').findAll('category');
      },

      filterByDateRange(startDate, endDate) {
        const store = get(this, 'store');

        // This prevents cached transactions from showing up when performing search.
        store.unloadAll('transaction');

        // store.query method is overriden to send a GET request at a custom URL.
        return store.query('transaction', { filterType: 'dateRange', startDate, endDate }).then(() => {
          return store.filter('transaction', transaction => transaction);
        });
      },

      addTransaction(transaction) {
        const newTransaction = get(this, 'store').createRecord('transaction', { ...transaction });

        newTransaction.save().then(() => {
          // Save worked
        }, (error) => {
          // TODO: Handle error
        });
      },

      splitTransaction(transaction, splits) {
        const parent_guid = get(transaction, 'guid');
        const date = get(transaction, 'date');
        const description = get(transaction, 'description');
        const account = get(transaction, 'account');
        const logo_url = get(transaction, 'logo_url');
        const memo = get(transaction, 'memo');

        const newTransactions = splits.map(split => {
          const { category_guid, category_name, splitAmount } = split;

          return {
            parent_guid,
            category_guid,
            category_name,
            amount: splitAmount,
            date,
            description,
            account,
            logo_url,
            memo
          };
        });

        for (let i = 0; i < newTransactions.length; i++) {
          const newTransaction = get(this, 'store').createRecord('transaction', { ...newTransactions[i] });

          // TODO: Need to think about this.
          newTransaction.save().then(() => {
            // Save worked
          }, (error) => {
            // Handle error
          });
        }
      },

      addCategory(category) {
        const newCategory = get(this, 'store').createRecord('category', { ...category });

        newCategory.save().then(() => {
          // Save worked
        }, (error) => {
          // TODO: Handle error
        });
      },

      getChildRows(row) {
        const guid = get(row, 'guid');

        return get(this, 'store').filter('transaction', transaction => {
          return get(transaction, 'parent_guid') === guid;
        });
      }
    }
});
