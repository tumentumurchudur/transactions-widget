import Ember from 'ember';
import computed, { reads, map } from 'ember-computed-decorators';
import EmberValidator from 'ember-validator';

const {
  Component,
  set,
  get,
  setProperties
} = Ember;

/**
 * This represents add-transaction component which allows
 * users to add manual transactions.
 *
 * @module add-transaction
 * @augments ember/Component
 */
export default Component.extend(EmberValidator, {
  /**
   * Defines CSS class.
   *
   * @property classNames
   * @type {Array}
   */
  classNames: ['add-transaction'],

  /**
  * Max # of characters allowed for memo property.
  *
  * @property maxMemoChars
  * @type {Number}
  * @default 40
  */
  maxMemoChars: 40,

  /**
  * Amount property of transaction.
  *
  * @property amount
  * @type {Number}
  * @default null
  */
  amount: null,

  /**
  * Description property of transaction.
  *
  * @property description
  * @type {String}
  * @default null
  */
  description: null,

  /**
  * Date property of transaction.
  *
  * @property date
  * @type {Date}
  * @default new Date()
  */
  date: new Date(),

  /**
  * Category property of transaction.
  *
  * @property category
  * @type {String}
  * @default null
  */
  category: null,

  /**
  * Account property of transaction.
  *
  * @property account
  * @type {String}
  * @default null
  */
  account: null,

  /**
  * Memo property of transaction.
  *
  * @property memo
  * @type {String}
  * @default null
  */
  memo: null,

  /**
  * selectedCategory object represents category_name and category_guid
  * properties of transaction.
  *
  * @property selectedCategory
  * @type {Object}
  * @default null
  */
  selectedCategory: null,

  /**
   * Determines whether amount field has validation errors.
   *
   * @type {String}
   * @default null
   */
  amountValidationErrors: null,

  /**
   * Determines whether description field has validation errors.
   *
   * @type {String}
   * @default null
   */
  descriptionValidationErrors: null,

  /**
   * Determines whether date field has validation errors.
   *
   * @type {String}
   * @default null
   */
  dateValidationErrors: null,

  /**
  * Keeps track of remaining chars for memo property of transaction.
  *
  * @property remainingChars
  * @type {number}
  */
  @reads('maxMemoChars') remainingChars,

  /**
  * Determines whether Save button should be enabled
  * based on missing fields and validation errors.
  *
  * @property isAddButtonDisabled
  * @type {Boolean}
  */
  @computed('amountValidationErrors', 'descriptionValidationErrors', 'dateValidationErrors', 'selectedCategory', 'account')
  isAddButtonDisabled(amountErrors, descriptionErrors, dateErrors, selectedCategory, account) {
    return amountErrors || descriptionErrors || dateErrors || !selectedCategory || !account;
  },

 /**
  * Creates a transaction object from the populated fields.
  *
  * @property transaction
  * @type {Array}
  */
  @computed('amount', 'description', 'date', 'selectedCategory.guid', 'selectedCategory.name', 'account', 'memo')
  transaction(amount, description, date, category_guid, category_name, account, memo) {
    return { amount, description, date, category_guid, category_name, account, memo };
  },

 /**
  * An array of objects containing name and guid of each category.
  *
  * @property allCategories
  * @type {Array}
  */
  @map('categories', category => {
    return { guid: category.get('guid'), name: category.get('name') };
  }) allCategories,

  /**
   * Sets focus in the amount field.
   *
   * @method didInsertElement
   */
  didInsertElement() {
    this._super(...arguments);

    Ember.$('#tran-amount').focus();
  },

  actions: {
    /**
     * Sets memo property of transaction and
     * updates remaining characters.
     *
     * @method changeMemo
     * @param  {String} memo
     */
    changeMemo(memo) {
      const remainingChars = get(this, 'maxMemoChars') - memo.length;

      setProperties(this, { memo, remainingChars })
    },

    /**
     * Sets date property of transaction.
     *
     * @method selectDate
     * @param {Date} date
     */
    selectDate(date) {
      set(this, 'date', date);
    },

    /**
     * Sets selectedCategory object.
     *
     * @method selectCategory
     * @param  {String} categoryGuid
     */
    selectCategory(categoryGuid) {
      const categories = get(this, 'allCategories');
      const selectedCategory = categories.filter(category => category.guid === categoryGuid)[0];

      set(this, 'selectedCategory', selectedCategory);
    },

    /**
     * Sets account property of transaction.
     *
     * @method selectAccount
     * @param  {String} account
     */
    selectAccount(account) {
      set(this, 'account', account);
    },

    /**
     * Validates amount value.
     *
     * @method validateAmount
     * @param  {String} amount
     */
    validateAmount(amount) {
      const model = Ember.Object.create({ amount });
      const validations = {
        amount: {
          required: {
            message: "Please enter amount"
          },
          numeric: {
            message: 'Amount must be numeric'
          }
        }
      };
      const validate = get(this, 'validateMap').bind(this, { model, validations });

      validate().then(() => {
        set(this, 'amountValidationErrors', null);
      }).catch(result => {
        set(this, 'amountValidationErrors', get(result, 'amount.errors'));
      });
    },

    /**
     * Validates description value.
     *
     * @method validateDescription
     * @param  {String} description
     */
    validateDescription(description) {
      const model = Ember.Object.create({ description });
      const validations = {
        description: {
          required: {
            message: "Please enter description"
          },
          length: {
            minimum: 4,
            messages: {
              minimum: 'Description is minimum of 4 characters'
            }
          }
        }
      };
      const validate = get(this, 'validateMap').bind(this, { model, validations });

      validate().then(() => {
        set(this, 'descriptionValidationErrors', null);
      }).catch(result => {
        set(this, 'descriptionValidationErrors', get(result, 'description.errors'));
      });
    },

    /**
     * Validates date value.
     *
     * @method validateDate
     */
    validateDate() {
      const model = Ember.Object.create({ date: get(this, 'date') });
      const validations = {
        date: {
          required: {
            message: "Please enter date"
          }
        }
      };
      const validate = get(this, 'validateMap').bind(this, { model, validations });

      validate().then(() => {
        set(this, 'dateValidationErrors', null);
      }).catch(result => {
        set(this, 'dateValidationErrors', get(result, 'date.errors'));
      });
    },

    /**
     * Closes the add-transaction component.
     *
     * @method cancel
     */
    cancel() {
      get(this, 'onCancel')();
    },

    /**
     * Adds a new transaction.
     *
     * @method add
     */
    add() {
      const isAmountValid = !get(this, 'amountValidationErrors');
      const isDescriptionValid = !get(this, 'descriptionValidationErrors');
      const isDateValid = !get(this, 'dateValidationErrors');
      const isAddButtonDisabled = get(this, 'isAddButtonDisabled');

      if (isAmountValid && isDescriptionValid && isDateValid && !isAddButtonDisabled) {
        get(this, 'onAdd')(get(this, 'transaction'));
      }
    }
  }
});
