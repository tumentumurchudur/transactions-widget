import Ember from 'ember';

const {
  Component,
  set,
  get,
  setProperties
} = Ember;

export default Component.extend({
  classNames: ['child-transaction-row'],

  actions: {
    changeDescription(description) {
      set(this, 'row.description', description);
    },

    changeAmount(amount) {
      set(this, 'row.amount', amount);
    },

    showCategorySelector() {
      set(this, 'showCategorySelector', true);
    },

    closeCategorySelector() {
      set(this, 'showCategorySelector', false);
    },

    updateCategory(category) {
      setProperties(get(this, 'row'), {
        category_guid: get(category, 'guid'),
        category_name: get(category, 'name')
      });
      set(this, 'showCategorySelector', false);
    }
  }
});
