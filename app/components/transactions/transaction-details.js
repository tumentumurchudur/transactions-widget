import Ember from 'ember';
import { reads, equal } from 'ember-computed-decorators';

const {
  Component,
  set,
  get,
  setProperties
} = Ember;

export default Component.extend({
  classNames: ['transaction-details'],

  row: {},
  childRows: [],
  showMenu: false,
  showExcludeModal: false,
  showSplitModal: false,
  arrowClass: 'caret-up',
  editRow: false,
  editChildRow: false,
  dateFormat: 'MMM DD, YYYY',

  @reads('row.description') description,
  @reads('row.date') date,
  @reads('row.memo') memo,
  @equal('childRows.length', 0) noChildRows: false,

  didInsertElement() {
    this._super(...arguments);

    // Sets the component in focus, so it can be closed when esc key is pressed.
    return this.$().attr({ tabindex: 1 }), this.$().focus();
  },

  keyDown(e) {
    // Check for esc key press on keydown.
    if (e.keyCode === 27) {
      get(this, 'onClose')();
    }
  },

  closeMenu() {
    const isMenuOpen = get(this, 'showMenu');

    if (isMenuOpen) {
      setProperties(this, { showMenu: false, arrowClass: 'caret-up' });
    }
  },

  init() {
    this._super(...arguments);

    const row = get(this, 'row');

    get(this, 'onRequestChildRows')(row).then(childRows => {
      set(this, 'childRows', childRows);
    });
  },

  actions: {
    close() {
      get(this, 'onClose')();
    },

    selectOption(row, e, action) {
      e.stopPropagation();

      switch(action) {
        case 'exclude':
          set(this, 'showExcludeModal', true);
          break;
        case 'edit':
          set(this, 'editRow', true);
          break;
        case 'split':
          set(this, 'showSplitModal', true);
          break;
        case 'remove-split':
          let allSplitsDeleted = true;

          // Delete all child rows.
          get(this, 'childRows').forEach(childRow => {
            childRow.destroyRecord().then(() => {
              // child row deleted.
            }, error => {
              allSplitsDeleted = false;
            });
          });

          // Update the parent row after all child rows are deleted.
          if (allSplitsDeleted) {
            set(this, 'row.has_been_split', false);
            get(this, 'row').save().then(() => {
              // Save worked.
            }, error => {
              // Handle error.
            });
          }
          break;
        case 'edit-split':
          set(this, 'editChildRow', true);
          break;
        default:
          break;
      }

      this.closeMenu();
    },

    selectDate(date) {
      set(this, 'date', date);
    },

    cancelEdit() {
      setProperties(this, {
        description: get(this, 'row.description'),
        date: get(this, 'row.date'),
        memo: get(this, 'row.memo'),
        editRow: false
      });
    },

    cancelEditChildRows() {
      set(this, 'editChildRow', false);
    },

    update(row) {
      setProperties(row, {
        date: get(this, 'date'),
        description: get(this, 'description'),
        memo: get(this, 'memo')
      });
      row.save();

      set(this, 'editRow', false);
    },

    updateChildRows() {
      let rowsUpdated = [];

      get(this, 'childRows').forEach((childRow, index) => {
        childRow.save().then(() => {
          rowsUpdated[index] = true;
        }, (error) => {
          rowsUpdated[index] = false;
        });
      });

      if (rowsUpdated.every(v => v)) {
        set(this, 'editChildRow', false);
      }
    },

    toggleMenu(e) {
      e.stopPropagation();

      const isMenuOpen = this.toggleProperty('showMenu');

      set(this, 'arrowClass', isMenuOpen ? 'caret-down' : 'caret-up');
    },

    closeMenu() {
      this.closeMenu();
    },

    closeExcludeModal() {
      set(this, 'showExcludeModal', false);
    },

    closeSplitModal() {
      set(this, 'showSplitModal', false);
    },

    excludeTransaction(row) {
      get(this, 'onExclude')(row);
    },

    splitTransaction(row, splits) {
      get(this, 'onSplit')(row, splits);
      set(this, 'showSplitModal', false);
    }
  }
});
