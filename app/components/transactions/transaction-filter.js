import Ember from 'ember';

const {
  Component,
  get,
  set,
  $
} = Ember;

export default Component.extend({
  results: [],
  showSearchOption: false,
  showAddTransaction: false,
  loading: false,
  hasManualAccount: false,

  init() {
    this._super(...arguments);

    get(this, 'filter')('').then(results => set(this, 'results', results));
    get(this, 'loadAccounts')().then(accounts => {
      const manualAccounts = accounts.filter(account => get(account, 'is_manual'));

      if (manualAccounts.length) {
        set(this, 'hasManualAccount', true);
      }

      set(this, 'accounts', accounts);
    });
    get(this, 'loadCategories')().then(categories => set(this, 'categories', categories));
  },

  actions: {
    applyFilterByKeyword(value) {
      get(this, 'filter')(value).then(results => set(this, 'results', results));
    },

    applyFilterByDateRange(startDate, endDate) {
      set(this, 'loading', true);
      get(this, 'filterByDateRange')(startDate, endDate).then(results => {
        set(this, 'results', results);
        set(this, 'loading', false);
      });
    },

    showSearchOption() {
      const isSearchShowing = this.toggleProperty('showSearchOption');

      // Sets focus in the search input.
      if(isSearchShowing) {
        Ember.run.scheduleOnce('afterRender', this, function() {
          $('#search-input').focus();
        });
      }
    },

    showAddTransaction() {
      set(this, 'showAddTransaction', true);
    },

    closeAddTransaction() {
      set(this, 'showAddTransaction', false);
    },

    addTransaction(transaction) {
      get(this, 'addTransaction')(transaction);
      set(this, 'showAddTransaction', false);
    }
  }
});
