import Ember from 'ember';

const {
  Component,
  get
} = Ember;

export default Component.extend({
  classNames: ['manage-transaction-menu', 'menu-arrow-box'],

  actions: {
    clickItem(e) {
      const action = e.target.getAttribute('data-action-value');

      get(this, 'onSelect')(e, action);
    }
  }
});
