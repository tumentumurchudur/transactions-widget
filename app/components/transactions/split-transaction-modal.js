import Ember from 'ember';
import computed, { reads, lt, or } from 'ember-computed-decorators';

const {
  Component,
  get,
  set,
  assign,
  setProperties
} = Ember;

export default Component.extend({
  classNames: ['split-transaction-modal'],

  splits: [],
  showCategorySelectorBase: false,
  showCategorySelector: [],

  @computed('splits', 'row.amount')
  remainingAmount(splits, origAmount) {
    const splitsTotal = splits.length ? splits.reduce((prev, next) => {
      return { splitAmount: prev.splitAmount + next.splitAmount };
    }) : 0;

    return origAmount - (splitsTotal.splitAmount || 0);
  },

  @lt('remainingAmount', 0) inValidSplit: false,
  @or('inValidSplit', 'noSplitFound') disableSave: false,

  @computed('remainingAmount', 'row.amount')
  noSplitFound(remainingAmount, amount) {
    return remainingAmount === amount;
  },

  @computed('categories', 'row.category_guid')
  baseCategory(categories, guid) {
    return categories.filter(category => get(category, 'guid') === guid)[0];
  },

  init() {
    this._super(...arguments);

    const defaultSplit = {
      category_guid: get(this, 'row.category_guid'),
      category_name: get(this, 'row.category_name'),
      splitAmount: 1
    };

    setProperties(this, { splits: [defaultSplit], showCategorySelector: [false] });
  },

  _closeCategorySelector(index) {
    if (index !== undefined) {
      const showCategorySelector = assign([], get(this, 'showCategorySelector'));

      showCategorySelector.removeAt(index);
      showCategorySelector.insertAt(index, false);

      set(this, 'showCategorySelector', showCategorySelector);
    } else {
      set(this, 'showCategorySelectorBase', false);
    }
  },

  actions: {
    save(row) {
      set(this, 'row.has_been_split', true);
      row.save().then(() => {
        const splits = get(this, 'splits');
        const baseCategory = get(this, 'baseCategory');
        const category_guid = get(baseCategory, 'guid');
        const category_name = get(baseCategory, 'name');
        const splitAmount = get(this, 'remainingAmount');
        const baseSplit = { category_guid, category_name, splitAmount };

        splits.pushObject(baseSplit);
        get(this, 'onSplit')(row, splits);
      });
    },

    cancel() {
      get(this, 'onCancel')();
    },

    removeSplit(index) {
      const splits = assign([], get(this, 'splits'));

      splits.removeAt(index);

      set(this, 'splits', splits);
    },

    addSplit() {
      const splits = assign([], get(this, 'splits'));
      const baseCategory = get(this, 'baseCategory');
      const category_guid = get(baseCategory, 'guid');
      const category_name = get(baseCategory, 'name');
      const newSplit = { category_guid, category_name, splitAmount: 1 };

      splits.pushObject(newSplit);

      set(this, 'splits', splits);
      get(this, 'showCategorySelector').pushObject(false);
    },

    updateBaseCategory(category) {
      const guid = get(category, 'guid');
      const name = get(category, 'name');
      const splitAmount = get(this, 'remainingAmount');
      const baseCategory = { guid, name, splitAmount };

      set(this, 'baseCategory', baseCategory);
      this._closeCategorySelector();
    },

    updateCategory(index, category) {
      const splits = assign([], get(this, 'splits'));
      const category_guid = get(category, 'guid');
      const category_name = get(category, 'name');
      const splitAmount = get(splits[index], 'splitAmount');
      const currentSplit = { category_guid, category_name, splitAmount };

      splits.removeAt(index);
      splits.insertAt(index, currentSplit);

      set(this, 'splits', splits);
      this._closeCategorySelector(index);
    },

    showCategorySelector(index) {
      if (index !== undefined) {
        const showCategorySelector = assign([], get(this, 'showCategorySelector'));

        showCategorySelector.removeAt(index);
        showCategorySelector.insertAt(index, true);

        set(this, 'showCategorySelector', showCategorySelector);
      } else {
        set(this, 'showCategorySelectorBase', true);
      }
    },

    closeCategorySelector(index) {
      if (index !== undefined) {
        this._closeCategorySelector(index);
      } else {
        set(this, 'showCategorySelectorBase', false);
      }
    },

    splitAmountChange(index, e) {
      const splits = assign([], get(this, 'splits'));
      const splitAmount = +e.target.value;
      const currentSplit = splits[index];

      set(currentSplit, 'splitAmount', splitAmount);

      splits.removeAt(index);
      splits.insertAt(index, currentSplit);
      set(this, 'splits', splits);
    }
  }
});
