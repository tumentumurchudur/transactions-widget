import Ember from 'ember';
import computed from 'ember-computed-decorators';

const {
  Component,
  get,
  set,
  setProperties
} = Ember;

/**
 * This represents each row in the transaction-table component.
 *
 * @module
 * @augments ember/Component
 */
export default Component.extend({
  /**
  * Defines the tag element that wraps this component.
  *
  * @property tagName
  * @type {string}
  * @default ""
  */
  tagName: '',

  /**
  * Determines if transaction-details component should be visible.
  *
  * @property showDetails
  * @type {Boolean}
  * @default false
  */
  showDetails: false,

  /**
  * Determines if category-selector component should be visible.
  *
  * @property showCategorySelector
  * @type {Boolean}
  * @default false
  */
  showCategorySelector: false,

  /**
   * Determines whether to display amount or balance field.
   *
   * @property monetaryColumn
   * @type {Number}
   */
  @computed('displayPage', 'row')
  monetaryColumn(page, row) {
    return page === 'transaction' ? get(row, 'amount') : get(row, 'balance');
  },

  actions: {
    /**
    * Toggles showDetails property.
    *
    * @method showDetails
    */
    showDetails() {
      this.toggleProperty('showDetails');
    },

    /**
     * Sets the showCategorySelector flag to true which
     * shows the category-selector component.
     *
     * @method showCategorySelector
     */
    showCategorySelector() {
      set(this, 'showCategorySelector', true);
    },

    /**
    * Sets the showCategorySelector flag to false which
    * hides the category-selector component.
    *
    * @method closeCategorySelector
    */
    closeCategorySelector() {
      set(this, 'showCategorySelector', false);
    },

    /**
    * Updates the category_name and category_guid fields
    * in the transaction model and closes the category-selector component.
    *
    * @method updateCategory
    */
    updateCategory(category) {
      const selectedRow = get(this, 'row');

      setProperties(selectedRow, {
        category_name: get(category, 'name'),
        category_guid: get(category, 'guid')
      });

      selectedRow.save().then(() => {
        set(this, 'showCategorySelector', false);
      });
    },

    /**
     * Excludes the selected transaction from the table.
     *
     * @method excludeTransaction
     * @param  {Object} currently selected row
     */
    excludeTransaction(row) {
      get(this, 'onExclude')(row);
    },

    splitTransaction(row, splits) {
      get(this, 'onSplit')(row, splits);
    },

    /**
     * Adds a new category.
     *
     * @method addCategory
     * @param {Object} category
     */
    addCategory(category) {
      get(this, 'onAddCategory')(category);
    },

    requestChildRows(row) {
      return get(this, 'onRequestChildRows')(row);
    }
  }
});
