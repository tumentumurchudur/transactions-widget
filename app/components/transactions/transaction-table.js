import Ember from 'ember';
import computed, { sort } from 'ember-computed-decorators';

const {
  Component,
  get,
  set
} = Ember;

export default Component.extend({
  classNames: ['transaction-table'],

  rows: [],
  showTotal: false,
  showTotalLabel: 'Total',
  displayPage: 'transaction',
  reverseSort: false,
  sortBy: 'date',

  @sort('rows', 'sortDefinition') sortedRows,

  @computed('displayPage')
  headers(page) {
    return ['date', 'description', 'category', 'account', page === 'transaction' ? 'amount' : 'balance'];
  },

  @computed('sortBy', 'reverseSort')
  sortDefinition(sortBy, reverseSort) {
    const sortOrder = reverseSort ? 'desc' : 'asc';

    return [`${sortBy}:${sortOrder}`];
  },

  @computed('rows', 'rows.length')
  totalAmount(rows, len) {
    let total = 0;

    for (let i = 0; i < len; i++) {
      const isAChildRow = get(rows.objectAt(i), 'parent_guid');

      if (!isAChildRow) {
        total += get(rows.objectAt(i), 'amount');
      }
    }

    return total;
  },

  actions: {
    sort(header) {
      set(this, 'sortBy', header);
      this.toggleProperty('reverseSort');
    },

    excludeTransaction(row) {
      const rows = get(this, 'rows').filter(r => r.id !== row.id);
      set(this, 'rows', rows);

      row.set('is_hidden', true);
      row.save();
    },

    splitTransaction(row, splits) {
      get(this, 'onSplit')(row, splits);
    },

    addCategory(category) {
      get(this, 'onAddCategory')(category);
    },

    requestChildRows(row) {
      return get(this, 'onRequestChildRows')(row);
    }
  }
});
