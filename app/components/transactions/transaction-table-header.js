import Ember from 'ember';

const {
  Component,
  get,
  set
} = Ember;

export default Component.extend({
  tagName: '',

  header: '',
  sortBy: '',
  reverseSort: false,
  sortHeader: '',

  init() {
    this._super(...arguments);

    const header = get(this, 'header');
    const sortHeader = header === 'category' ? 'category_name' : header;

    set(this, 'sortHeader', sortHeader);
  },

  actions: {
    click(header) {
      get(this, 'onClick')(header);
    }
  }
});
