import Ember from 'ember';
import { reads } from 'ember-computed-decorators';

const {
  Component,
  get,
  set
} = Ember;

/**
 * This represents the category-selector component.
 *
 * @module category-selector
 * @augments ember/Component
 */
export default Component.extend({
  /**
   * Defines css class.
   *
   * @property classNames
   * @type {Array}
   */
  classNames: ['category-selector', 'arrow-box'],

  /**
   * Determines if consumer of this component is allowed to add categories.
   *
   * @property allowAddCategory
   * @type {Boolean}
   */
  allowAddCategory: true,

  /**
   * Keeps track of filtered categories as users perform search.
   *
   * @property filteredCategories
   * @type {Object}
   */
  @reads('categories') filteredCategories: null,

  actions: {
    /**
     * Filters categories by keyword and
     * sets filteredCategories property.
     *
     * @method filter
     * @param  {String} value input typed by user
     */
    filter(value) {
      const categories = get(this, 'categories').filter(category => {
        return category.get('name').toLowerCase().indexOf(value.toLowerCase()) > -1;
      });

      set(this, 'filteredCategories', categories);
    },

    /**
     * Updates category with the category selected from the dropdown.
     *
     * @method selectCategory
     * @param  {String} category
     * @param  {MouseEvent} e
     */
    selectCategory(category, e) {
      e.stopPropagation();

      get(this, 'onCategorySelect')(category);
    },

    /**
     * Adds a new category.
     *
     * @method addCategory
     */
    addCategory() {
      const name = get(this, 'newCategoryName');
      const guid = 'CATEGORY_GUID_ADD_' + Math.random(); // TODO: Remove when in integration.
      const created_at = new Date();
      const newCategory = { name, guid, created_at };

      get(this, 'onAddCategory')(newCategory);

      set(this, 'newCategoryName', null);
    }
  }
});
