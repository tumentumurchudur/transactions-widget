import Ember from 'ember';

const {
  Component,
  get
} = Ember;

export default Component.extend({
  classNames: ['exclude-transaction-modal'],

  row: {},

  actions: {
    exclude(row) {
      get(this, 'onExclude')(row);
    },

    cancel() {
      get(this, 'onCancel')();
    }
  }
});
