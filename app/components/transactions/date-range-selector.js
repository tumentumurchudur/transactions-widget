import Ember from 'ember';
import computed from 'ember-computed-decorators';

const {
  Component,
  get,
  set,
  setProperties
} = Ember;

/**
 * TODO: Include static copy into our content service as it will
 * allow for language translation in future iterations.
 */
const CURRENT_MONTH = 'Current month';
const PREV_MONTH = 'Previous month';
const PREV_30_DAYS = 'Previous 30 days';
const LAST_12_MONTHS = 'Last 12 months';

export default Component.extend({
  classNames: ['date-range-selector'],

  showOptions: false,
  options: [],
  selectedOption: PREV_30_DAYS,
  caretClass: 'caret-up',

  @computed('options', 'selectedOption')
  availOptions(options, selectedOption) {
    return options.filter(option => option !== selectedOption);
  },

  init() {
    this._super(...arguments);

    set(this, 'options', [CURRENT_MONTH, PREV_MONTH, PREV_30_DAYS, LAST_12_MONTHS]);
  },

  actions: {
    showOptions() {
      setProperties(this, { showOptions: true, caretClass: 'caret-down' });
    },

    closeOptions() {
      setProperties(this, { showOptions: false, caretClass: 'caret-up' });
    },

    select(option) {
      const dateFormat = 'M-D-YYYY';
      let startDate = '';
      let endDate = '';

      if (option === CURRENT_MONTH) {
        const date = new Date(), y = date.getFullYear(), m = date.getMonth();

        startDate = new Date(y, m, 1);
        endDate = new Date(y, m + 1, 0);
      } else if (option === PREV_MONTH) {
          const date = new Date(), y = date.getFullYear(), m = date.getMonth() - 1;

          startDate = new Date(y, m, 1);
          endDate = new Date(y, m + 1, 0);
      } else if (option === PREV_30_DAYS) {
          endDate = new Date();
          startDate = moment().add(-30, 'days');
      } else if (option === LAST_12_MONTHS) {
          endDate = new Date();
          startDate = moment().add(-12, 'months');
      }

      get(this, 'onSelect')(moment(startDate).format(dateFormat), moment(endDate).format(dateFormat));

      setProperties(this, { selectedOption: option, showOptions: false, caretClass: 'caret-up' });
    }
  }
});
