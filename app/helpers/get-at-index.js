import Ember from 'ember';

const {
  isArray
} = Ember;

export function getAtIndex(params/*, hash*/) {
  const paramArray = params[0];
  const paramIndex = params[1];

  if (isArray(paramArray) && paramIndex >= 0) {
    return paramArray[paramIndex];
  }
}

export default Ember.Helper.helper(getAtIndex);
