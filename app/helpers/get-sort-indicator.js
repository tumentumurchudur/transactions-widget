import Ember from 'ember';

export function getSortIndicator(params) {
  const [column, sortBy, reverseSort] = params;

  return column === sortBy && reverseSort ? 'arrow-up' : 'arrow-down';
}

export default Ember.Helper.helper(getSortIndicator);
