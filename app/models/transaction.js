import DS from 'ember-data';
import Model from 'ember-data/model';

export default Model.extend({
  date: DS.attr('date'),
  description: DS.attr('string'),
  category_name: DS.attr('string'),
  category_guid: DS.attr('string'),
  account: DS.attr('string'),
  amount: DS.attr('number'),
  logo_url: DS.attr('string'),
  is_hidden: DS.attr('boolean'),
  memo: DS.attr('string'),
  guid: DS.attr('string'),
  parent_guid: DS.attr('string'),
  has_been_split: DS.attr('boolean'),
  balance: DS.attr('number')
});
