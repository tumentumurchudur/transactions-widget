import DS from 'ember-data';
import Ember from 'ember';

const {
  RSVP,
  $
} = Ember;

export default DS.RESTAdapter.extend({
  namespace: 'api',

  query(store, type, query) {
    if (query.filterType && query.filterType === 'dateRange') {
      // This is a custom URL and can be changed as needed.
      const url = `${this.namespace}/transactions/from/${query.startDate}/to/${query.endDate}`;

      return new RSVP.Promise(function(resolve, reject) {
        // TODO: Import jQuery as a separate module because future versions of Ember
        // will likely require jQuery to be imported explicitly as a module.
        $.getJSON(url).then(data => resolve(data), err => reject(err));
      });
    } else {
      // Default action is still in place for all other queries.
      return this._super(store, type, query);
    }
  }
});
