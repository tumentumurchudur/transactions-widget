import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('transactions/split-transaction-modal', 'Integration | Component | transactions/split transaction modal', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{transactions/split-transaction-modal}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#transactions/split-transaction-modal}}
      template block text
    {{/transactions/split-transaction-modal}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
