import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('transactions/child-transaction-row', 'Integration | Component | transactions/child transaction row', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{transactions/child-transaction-row}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#transactions/child-transaction-row}}
      template block text
    {{/transactions/child-transaction-row}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
