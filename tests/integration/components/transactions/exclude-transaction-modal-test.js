import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('transactions/exclude-transaction-modal', 'Integration | Component | transactions/exclude transaction modal', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{transactions/exclude-transaction-modal}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#transactions/exclude-transaction-modal}}
      template block text
    {{/transactions/exclude-transaction-modal}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
