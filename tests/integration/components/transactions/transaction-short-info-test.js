import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('transactions/transaction-short-info', 'Integration | Component | transactions/transaction short info', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{transactions/transaction-short-info}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#transactions/transaction-short-info}}
      template block text
    {{/transactions/transaction-short-info}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
