import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('transaction/manage-transaction-menu', 'Integration | Component | transaction/manage transaction menu', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{transaction/manage-transaction-menu}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#transaction/manage-transaction-menu}}
      template block text
    {{/transaction/manage-transaction-menu}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
