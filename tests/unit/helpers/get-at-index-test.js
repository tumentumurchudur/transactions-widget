
import { getAtIndex } from 'ally-transaction/helpers/get-at-index';
import { module, test } from 'qunit';

module('Unit | Helper | get at index');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getAtIndex([42]);
  assert.ok(result);
});

