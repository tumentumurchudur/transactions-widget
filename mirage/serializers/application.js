import { RestSerializer } from 'ember-cli-mirage';
const { underscore } = Ember.String;

export default RestSerializer.extend({
  // This removes the default camelCase and changes it to snake case.
  keyForAttribute(attr) {
    return underscore(attr);
  }
});
