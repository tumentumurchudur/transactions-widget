import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  'date'() { return faker.date.between(new Date('03/01/2017'), new Date('5/15/2017')); },
  'description'() { return faker.lorem.sentence(3); },
  'category'() { return faker.lorem.words(2); },
  'category_name'() { return faker.lorem.words(2); },
  'category_guid'() { return faker.random.uuid(); },
  'account'() { return faker.lorem.words(2); },
  'amount'() { return parseFloat(faker.finance.amount(100, 2500)); },
  logo_url: faker.internet.avatar,
  is_hidden: false,
  memo: 'memo',
  'guid'() { return faker.random.uuid(); },
  parent_guid: null,
  has_been_split: false,
  'balance'() { return parseFloat(faker.finance.amount(500, 5000)); }
});
