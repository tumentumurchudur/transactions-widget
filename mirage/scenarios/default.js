export default function(server) {
  server.createList('account', 5);

  const categories = [];

  for (let i = 0; i < 10; i++) {
    categories[i] = server.create('category');
  }

  for (let i = 0; i < 25; i++) {
    const index = Math.floor(Math.random() * 9) + 0
    const { guid, name } = categories[index];

    server.create('transaction', { category_guid: guid, category_name: name });
  }

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.

    Make sure to define a factory for each model you want to create.
  */

  // server.createList('post', 10);
}
